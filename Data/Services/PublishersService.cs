using BookAPI.Data.Models;
using BookAPI.Data.ViewModels;

namespace BookAPI.Data.Services;

public class PublishersService
{
    private AppDbContext _context;

    public PublishersService(AppDbContext context)
    {
        _context = context;
    }

    public void AddPublisher(PublisherVM publisher)
    {
        var _publisher = new Publisher()
        {
            Name = publisher.Name
        };
        _context.Publishers.Add(_publisher);
        _context.SaveChanges();
    }

    public PublisherWithBooksAndAuthorsVM GetPublisherData(int publisherId)
    {
        var _publisherData = _context.Publishers.Where(p => p.Id == publisherId).Select(n => new PublisherWithBooksAndAuthorsVM()
        {
            Name = n.Name,
            BookAuthors = n.Books.Select(b => new BookAuthorVM()
            {
                BookNames = b.Title,
                BookAuthors = b.Book_Authors.Select(a => a.Author.FullName).ToList()
            }).ToList()
        }).FirstOrDefault();

        return _publisherData;
    }

    public void DeletePublisherById(int publisherId)
    {
        var _publisher = _context.Publishers.FirstOrDefault(n => n.Id == publisherId);
        if (_publisher != null)
        {
            _context.Publishers.Remove(_publisher);
            _context.SaveChanges();
        }
    }
}