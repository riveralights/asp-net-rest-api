namespace BookAPI.Data.Models;

public class Publisher 
{
    public int Id { get; set; }
    public string Name { get; set; }

    // Navigation Properties
    // One publisher have many books
    // Many books have one publisher
    public List<Book> Books { get; set; }
}