using BookAPI.Data.Services;
using BookAPI.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BookAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PublishersController : ControllerBase
{
    private PublishersService _publishersService;

    public PublishersController(PublishersService publishersService)
    {
        _publishersService = publishersService;
    }

    [HttpPost("add-publisher")]
    public IActionResult AddPublisher([FromBody]PublisherVM publisher)
    {
        _publishersService.AddPublisher(publisher);
        return Ok();
    }

    [HttpGet("get-publisher-books-with-author/{id}")]
    public IActionResult GetPublisherData(int id)
    {
        var response = _publishersService.GetPublisherData(id);
        return Ok(response);
    }

    [HttpDelete("delete-publisher-by-id/{id}")]
    public IActionResult DeletePublisherById(int id)
    {
        _publishersService.DeletePublisherById(id);
        return Ok();
    }
}