using BookAPI.Data.Services;
using BookAPI.Data.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace BookAPI.Controllers;

[Route("api/[controller]")]
[ApiController]

public class AuthorsController : ControllerBase
{
    public AuthorsService _authorsService;

    public AuthorsController(AuthorsService authorsService)
    {
       _authorsService = authorsService;
    }

    [HttpPost("add-author")]
    public IActionResult AddAuthor([FromBody] AuthorVM author)
    {
        _authorsService.AddAuthor(author);
        return Ok();
    }

    [HttpGet("get-author-with-books/{id}")]
    public IActionResult GetAuthorWithBooks(int id)
    {
        var response = _authorsService.GetAuthorWithBooks(id);
        return Ok(response);
    }


}